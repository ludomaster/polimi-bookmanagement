//
//  main.c
//  BookManagement
//
//  Created by Ludovico Lavini on 01/12/14.
//  Copyright (c) 2014 Lavini Enterprises. All rights reserved.
//



#include <stdio.h>

#pragma mark defines


#define maxLibri 500
#define numeroAutori 10
#define lungtitolo 20
#define lungAutori 50
#define lungDesc 50
#define lungGenere 20
#define lungCE 20


#pragma mark Variabili Globali

typedef enum {false, true} boolean;

typedef struct {
    char title[lungtitolo];
    char autori[lungAutori];
    char casaEditrice[lungCE];
    char descrizione[lungDesc];
    char genere[lungGenere];
    int codiceNumerico;
    int annoDP;
    int numeroDiPagine;
    boolean valido;
}libro;


libro catalogo[maxLibri];

int numeroDiLibri = 0;

int p;


#pragma mark Dichiarazione Funzioni
boolean checkVuoto();
void clean();
int strLength(char stringa[]);
libro* cercaLibro(int codiceNumerico, boolean showerror);
void prettyPrint(char *separatore, char *nuovoSeparatore, char *prefisso, char stringa[]);
void scanStringa(int maxLunghezza, char nomeStringa[], char commentox[], char variabile[]);
void scanNumero(char nomeStringa[],char commento[], int *variabile);
int posizioneLibera();
void inserisciLibro();
void stampaLibro(libro *l, int posizione);
void stampaCatalogo();
void cancellaLibroInCatalogo(libro *l);
int selezioneOpzioni();

#pragma mark Funzioni Accessorie

//Funzione non molto dissimile da un middleware, verifica che il catalogo non sia vuoto prima di proseguire con le altre operazioni
boolean checkVuoto(){
    if (numeroDiLibri == 0) {
        printf("Catalogo vuoto, prego inserire un libro attraverso l'apposita funzione\n");
        return true;
    }else{
        return false;
    }
}



//pulizia dello schermo (Non funziona su Windows)
void clean() {
    printf("\e[1;1H\e[2J");
}



//calcolo della lunghezza di una stringa passata come parametro, fino al terminatore '\0'
int strLength(char stringa[]) {
    int lunghezza = 0;
    
    while (*stringa != '\0') {
        lunghezza++;
        stringa++;
    }
    
    return lunghezza;
}




/*
 Ricerca di uno specifico libro all'interno del catalogo utilizzando il codice univoco come query
 Parametri
 *codiceNumerico:   intero corrispondente al codice univoco di ciascun elemento del catalogo. Se è uguale a -1, ovviamente invalido, la funzione si occupa in modo interattivo di richiedere all'utente il codice del libro da cercare
 *showError:        boolean che se impostato a true avvisa l'utente in caso la ricerca vada a buon fine. Ciò è necessario specialmente in modalità interattiva
 */

libro* cercaLibro(int codiceNumerico, boolean showerror) {
    
    libro* libroTrovato = NULL;
    if (codiceNumerico == -1) {
        do{
            printf("Inserire codice numerico del libro: ");
        }while (scanf("%d", &codiceNumerico) != 1 && (p = getchar()) != '\n' && p != EOF);
        
    }
    
    
    for (int i = 0; i<maxLibri; i++) {
        if (catalogo[i].codiceNumerico == codiceNumerico && catalogo[i].valido == true) {
            return &catalogo[i];
        }
    }
    
    libro tempLibro;
    libroTrovato = &tempLibro;
    libroTrovato->valido = false;
    if (showerror) {
        printf("Libro non trovato per il codice numerico %d\n \n", codiceNumerico);
    }
    return libroTrovato;
}



/*Stampa formattata: la funzione accetta vari input
 
 Parametri
 * separatore: è il separatore della stringa in input che dev'essere sostituito
 * nuovoSeparatore: nuovo separatore che verrà stampato al posto di "separatore"
 * prefisso: eventuale prefisso ad ogni sottostringa stampata, ad esempio un elenco
 * stringa: stringa da stampare
 
 */

void prettyPrint(char *separatore, char *nuovoSeparatore, char *prefisso, char stringa[]){
    
    int lunghezza = strLength(stringa); //Necessaria l'allocazione statica per evitare che nello scorrere la stringa carattere per carattere nel loop for la lunghezza calcolata diminuisca ad ogni iterazione
    
    for (int i = 0; i<lunghezza; i++) {
        if(*stringa != *separatore) {
            if (i == 0) {
                if(prefisso)printf("%s", prefisso);
            }
            printf("%c", *stringa);
        }else if(*stringa == *separatore) {
            printf("%s", nuovoSeparatore);
            if (prefisso)printf("%s", prefisso);
        }
        stringa++;

    }
    
    printf("\n");
    
}



/*
 
 Particolare implementazione della scanf per le stringhe di testo, con validazione di lunghezza
 Parametri:
 * maxLunghezza:    lunghezza massima della stringa che il campo può accettare
 * nomeStringa:     titolo del campo di testo che la funzione richiede all'utente
 * commento:        un eventuale commento esplicativo sul valore che l'utente deve inserire
 * variabile:       puntatore alla variabile all'interno della quale la funzione inserisce il cahr
 
 */

void scanStringa(int maxLunghezza, char nomeStringa[], char commento[], char variabile[]) {
    while ((p = getchar()) != '\n' && p != EOF);
    
    if (commento) {
        printf("Inserire %s (Lunghezza max %d caratteri, %s): ", nomeStringa, maxLunghezza, commento);
    }else{
        printf("Inserire %s (Lunghezza max %d caratteri): ", nomeStringa, maxLunghezza);
    }
    while (scanf("%s", variabile)!=1 || strLength(variabile) >= maxLunghezza) {
        while ((p = getchar()) != '\n' && p != EOF);
        printf("Stringa non valida, reinserire: ");
    }
}

/*
 
 Particolare implementazione della scanf per i valori numerici, con validazione
 Parametri:
 * nomeStringa:     titolo del campo numerico che la funzione richiede all'utente
 * commento:        un eventuale commento esplicativo sul valore che l'utente deve inserire
 * variabile:       puntatore alla variabile all'interno della quale la funzione inserisce il valore
 
 */
void scanNumero(char nomeStringa[],char commento[], int *variabile) {
    
    if (commento) {
        printf("Inserire %s (%s): ", nomeStringa, commento);
    }else{
        printf("Inserire %s: ", nomeStringa);
    }
    while (scanf("%d", variabile) != 1 | *variabile < 0) {
        while ((p = getchar()) != '\n' && p != EOF);
        printf("Numero non valido, reinserire %s: ", nomeStringa);
    }

}


#pragma mark Funzioni Principali

//Ricerca posizione libera (elemento dell'array con "valido" impostato a falso)
int posizioneLibera() {
    boolean trovato = false;
    int posizione = 0;
    while (trovato == false && posizione < maxLibri) {
        if (catalogo[posizione].valido == false) {
            trovato = true;
        }
        posizione++;
    }
    
    
    return posizione-1;
}



/*
 Richiesta all'utente dell'inserimento di un nuovo libro, utilizzando le funzioni scanStringa e scanNumero per riempire i vari campi della struct libro
 Viene verificato che il codice univico sia, appunto, univoco, e che pertanto non sia presente in catalogo un altro libro con lo stesso codice
 
 */

void inserisciLibro(){
    libro tempLibro;
    printf("Inserimento nuovo libro\n");
    
    scanStringa(lungtitolo, "Titolo", NULL, tempLibro.title);
    
    scanStringa(lungAutori, "Autori","Separare gli autori con una virgola e utilizzare underscore _ al posto degli spazi",tempLibro.autori);
    
    scanStringa(lungCE, "Casa Editrice", NULL, tempLibro.casaEditrice);

    scanStringa(lungDesc, "Descrizione","Utilizzare gli underscore _ al posto degli spazi", tempLibro.descrizione);
    
    scanStringa(lungGenere, "Genere",NULL, tempLibro.genere);
    
    scanNumero("Anno di Pubblicazione", NULL, &tempLibro.annoDP);
    
    scanNumero("Numero di Pagine", NULL, &tempLibro.numeroDiPagine);
    
    boolean presente = false;
    
    do{
        if (presente) {
            printf("Libro già presente per questo codice, ritentare\n");
        }
        scanNumero("Codice Numerico Univoco","Non inserire un codice preceduto da zeri",&tempLibro.codiceNumerico);
        
    }while ((presente = (cercaLibro(tempLibro.codiceNumerico, false)->valido)) == true);
    
    tempLibro.valido = true;
    
    catalogo[posizioneLibera()] = tempLibro;
    numeroDiLibri++;
    
    printf("\n####Inserimento completato####\n \n");
    
}



/*
 Stampa del singolo libro, formattata e tabulata
 Parametri
 *l:            puntatore all'entità di tipo libro che dev'essere stampata
 *posizione:    utilizzato durante la stampa dell'intero catalogo, stampa un valore numerico che viene fatto corrispondere durante la chiamata della funzione alla posizione incrementale del libro nel catalogo, ignorando quindi gli elementi il cui campo "valido" è false
 */

void stampaLibro(libro *l, int posizione) {

    if (posizione >= 0) {
        printf("\n#Posizione %d\n", posizione);
    }else{
        printf("Stampa libro\n");
    }

    printf("Titolo: \t\t\t%s\n", l->title);

    
    printf("Autori:\n");
    prettyPrint(",", "\n", "\t\t\t\t* ", l->autori);
    

    printf("Casa Editrice:\t\t\t%s\n", l->casaEditrice);


    printf("Descrizione: \t\t\t");
    prettyPrint("_", " ", NULL, l->descrizione);

    
    printf("Genere: \t\t\t%s\n", l->genere);

    
    printf("Codice Numerico: \t\t%d\n", l->codiceNumerico);


    printf("Anno di Pubblicazione: \t\t%d\n", l->annoDP);

    
    printf("Numero di Pagine: \t\t%d\n", l->numeroDiPagine);

}



/*
    Iterazione all'interno dell'intero catalogo e stampa di tutti i libri contenuti (si tratta ovviamente di quelli con campo "valido" impostato a true)
 */
void stampaCatalogo() {
    printf("Stampa Intero Catalogo\n");
    int posValida = 0;
    for (int i = 0; i < maxLibri; ++i)
    {
        if (catalogo[i].valido == true) {
            stampaLibro(&catalogo[i], posValida);
            posValida++;
            printf("------------------------------------\n");
        }
    }
    printf("\n\n");
    
}



//Eliminazione libro dal catalogo, assume che l'esistenza(validità) del libro da eliminare sia già stata verificata e si occupa soltanto di marcarlo come non valido
void cancellaLibroInCatalogo(libro *l) {
    
    l->valido = false;
    printf("Eliminazione libro completata con successo\n");
    numeroDiLibri--;
        
}



//Visualizzazione del menù e richiesta di input operativo all'utente. Viene reiterata fino a quando non viene inserito un parametro valido.
int selezioneOpzioni(){
    
    int x = 0;
    p = 99;

    while (p != 1) {
        if (p != 99) {
            printf("Opzione non valida");
            while ((p = getchar()) != '\n' && p != EOF);
        }
        printf("\n\n###############\n");
        printf("Selezionare un'opzione\n");
        printf("1) Inserimento nuovo Libro\n");
        printf("2) Ricerca libro nel catalogo (per codice numerico)\n");
        printf("3) Cancellazione libro da catalogo\n");
        printf("4) Stampa a video del Catalogo\n");
        printf("0) Esci dal programma\n");
        printf("> ");
        
        p = scanf("%d", &x);
    }
    
    
    
    clean();

    switch (x) {
        case 0:
            return 5;
            break;
        case 1:
            if (numeroDiLibri >= maxLibri) {
                printf("Memoria piena, è necessario cancellare un elemento attraverso l'apposita funzione prima di poter inserire un ulteriore libro nel catalogo\n\n");
            }else{
                inserisciLibro();
            }
            break;
        case 2: {
            if (!checkVuoto()) {
                
                libro *risultatoQuery = cercaLibro(-1, true);
                
                if (risultatoQuery->valido == true) {
                    stampaLibro(risultatoQuery, -1);
                }
            }
            
        }
            break;
        case 3:
            if (!checkVuoto()) {
                libro *l = cercaLibro(-1, true);
                
                if (l->valido == true) {
                    cancellaLibroInCatalogo(l);
                }

            }
            break;
        case 4:
            if (!checkVuoto()) {
                stampaCatalogo();
            }
            break;
        default:
            printf("Opzione non valida \n");
            break;
    }
    
    selezioneOpzioni();
    
    return 0;
    
    
}



int main(int argc, const char * argv[]) {
        
    //Inizializzazione Rubrica, tutti gli struct contenuti nell'array vengono marcati come non validi e pertanto scrivibili dal programma
    for (int i = 0; i<maxLibri; i++) {
        catalogo[i].valido = false;
    }

    selezioneOpzioni();
    
}
